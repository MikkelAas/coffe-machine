package mikkel.coffeemachine.commands;

import net.dv8tion.jda.api.events.message.MessageReceivedEvent;

public class MakeCoffee extends Command {

    private String command;
    private String commandDesc;

    public MakeCoffee() {
        command = "makecoffee";
        commandDesc = "Makes coffee";
    }

    @Override
    public void run(MessageReceivedEvent event, String args) {
        event.getChannel().sendMessage("<@" + event.getAuthor().getId() + "> " +
                "Lag din egen kaffe, din person med gjennomgripende utviklingsforstyrrelser!").queue();
    }

    public String getCommand() {
        return this.command;
    }

    public String getCommandDesc() {
        return this.commandDesc;
    }
}

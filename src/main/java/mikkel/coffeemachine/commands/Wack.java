package mikkel.coffeemachine.commands;

import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.events.message.MessageReceivedEvent;

public class Wack extends Command{

    private String command;
    private String commandDesc;

    public Wack() {
        command = "wack";
        commandDesc = "The meme.";
    }

    public void run(MessageReceivedEvent event, String args) {
        EmbedBuilder eb = new EmbedBuilder();
        eb.setAuthor("Wack");
        eb.setImage("https://pbs.twimg.com/media/DV8LCKdU8AAnLKw?format=png");
        eb.setDescription("his hair, WACK. his gear, WACK. his jewelry, WACK. his foot stance, WACK. " +
               "the way that he talks, WACK. the way he doesn't even like to smile, WACK. me, i'm tight as FUCK.");
        event.getChannel().sendMessage(eb.build()).queue();
    }

    public String getCommand() {
        return this.command;
    }

    public String getCommandDesc() {
        return this.commandDesc;
    }
}
